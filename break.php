<?php
echo "<p><b>Example of using the Break statement:</b></p>";

for ($i=0; $i<=10; $i++)
{
    if ($i==3)
    {
        break;
    }
    echo "The number is ".$i;
    echo "<br />";
}
?>